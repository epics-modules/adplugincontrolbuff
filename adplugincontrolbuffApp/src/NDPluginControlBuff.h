#ifndef NDPluginControlBuff_H
#define NDPluginControlBuff_H

#include <epicsTypes.h>
#include <postfix.h>

#include "NDPluginDriver.h"
#include "NDArrayRing2.h"

/* Param definitions */
#define NDCtrlBuffControlString             "CTRL_BUFF_CONTROL"               /* (asynInt32,        r/w) Run scope? */
#define NDCtrlBuffStatusString              "CTRL_BUFF_STATUS"                /* (asynOctetRead,    r/o) Scope status */
#define NDCtrlBuffTriggerAString            "CTRL_BUFF_TRIGGER_A"             /* (asynOctetWrite,   r/w) Trigger A attribute name */
#define NDCtrlBuffTriggerBString            "CTRL_BUFF_TRIGGER_B"             /* (asynOctetWrite,   r/w) Trigger B attribute name */
#define NDCtrlBuffTriggerAValString         "CTRL_BUFF_TRIGGER_A_VAL"         /* (asynFloat64,      r/o) Trigger A value */
#define NDCtrlBuffTriggerBValString         "CTRL_BUFF_TRIGGER_B_VAL"         /* (asynFloat64,      r/o) Trigger B value */
#define NDCtrlBuffTriggerCalcString         "CTRL_BUFF_TRIGGER_CALC"          /* (asynOctetWrite,   r/w) Trigger calculation expression */
#define NDCtrlBuffTriggerCalcValString      "CTRL_BUFF_TRIGGER_CALC_VAL"      /* (asynFloat64,   r/o) Trigger calculation value */
#define NDCtrlBuffPresetTriggerCountString  "CTRL_BUFF_PRESET_TRIGGER_COUNT"  /* (asynInt32,        r/w) Preset number of triggers 0=infinite*/
#define NDCtrlBuffActualTriggerCountString  "CTRL_BUFF_ACTUAL_TRIGGER_COUNT"  /* (asynInt32,        r/w) Actual number of triggers so far */
#define NDCtrlBuffPreTriggerString          "CTRL_BUFF_PRE_TRIGGER"           /* (asynInt32,        r/w) Number of pre-trigger images */
#define NDCtrlBuffPostTriggerString         "CTRL_BUFF_POST_TRIGGER"          /* (asynInt32,        r/w) Number of post-trigger images */
#define NDCtrlBuffCurrentImageString        "CTRL_BUFF_CURRENT_IMAGE"         /* (asynInt32,        r/o) Number of the current image */
#define NDCtrlBuffPostCountString           "CTRL_BUFF_POST_COUNT"            /* (asynInt32,        r/o) Number of the current post count image */
#define NDCtrlBuffSoftTriggerString         "CTRL_BUFF_SOFT_TRIGGER"          /* (asynInt32,        r/w) Force a soft trigger */
#define NDCtrlBuffTriggeredString           "CTRL_BUFF_TRIGGERED"             /* (asynInt32,        r/o) Have we had a trigger event */
#define NDCtrlBuffFlushOnSoftTrigString     "CTRL_BUFF_FLUSH_ON_SOFTTRIGGER"  /* (asynInt32,        r/w) Flush buffer immediatelly when software trigger obtained */

#define NDCtrlBuffGetElementString          "CTRL_BUFF_POP"                   /* (asynInt32,        r/w) Run scope? */

/** Performs a scope like capture.  Records a quantity
  * of pre-trigger and post-trigger images
  */
class NDPLUGIN_API NDPluginControlBuff : public NDPluginDriver {
public:
    NDPluginControlBuff(const char *portName, int queueSize, int blockingCallbacks,
                 const char *NDArrayPort, int NDArrayAddr,
                 int maxBuffers, size_t maxMemory,
                 int priority, int stackSize);
    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);
    asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    asynStatus writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual);

    //template <typename epicsType> asynStatus doProcessControlBuffT(NDArray *pArray);
    //asynStatus doProcessControlBuff(NDArray *pArray);

protected:
    int NDCtrlBuffControl;
    #define FIRST_NDPLUGIN_CTRL_BUFF_PARAM NDCtrlBuffControl
    /* Scope */
    int NDCtrlBuffStatus;
    int NDCtrlBuffTriggerA;
    int NDCtrlBuffTriggerB;
    int NDCtrlBuffTriggerAVal;
    int NDCtrlBuffTriggerBVal;
    int NDCtrlBuffTriggerCalc;
    int NDCtrlBuffTriggerCalcVal;
    int NDCtrlBuffPresetTriggerCount;
    int NDCtrlBuffActualTriggerCount;
    int NDCtrlBuffPreTrigger;
    int NDCtrlBuffPostTrigger;
    int NDCtrlBuffCurrentImage;
    int NDCtrlBuffPostCount;
    int NDCtrlBuffSoftTrigger;
    int NDCtrlBuffTriggered;
    int NDCtrlBuffFlushOnSoftTrig;
    int NDCtrlBuffGetElement;

    void flushPreBuffer();
    void controlledFlush();

private:

    asynStatus calculateTrigger(NDArray *pArray, int *trig);
    NDArrayRing2 *preBuffer_;
    NDArray *pOldArray_;
    int previousTrigger_;
    int maxBuffers_;
    char triggerCalcInfix_[MAX_INFIX_SIZE];
    char triggerCalcPostfix_[MAX_POSTFIX_SIZE];
    double triggerCalcArgs_[CALCPERFORM_NARGS];
};

#endif

