/*
 * NDArrayRing2.cpp
 *
 * Circular ring of NDArray pointers
 * Author: Alan Greer
 *
 * Created June 21, 2013
 */

#include "NDArrayRing2.h"

NDArrayRing2::NDArrayRing2(int noOfBuffers)
{
  noOfBuffers_ = noOfBuffers;
  buffers_ = NULL;
  writeIndex_ = -1;
  readIndex_ = -1;
  wrapped_ = 0;
  size_ = 0;

  buffers_ = new NDArray *[noOfBuffers_];
  for (int index = 0; index < noOfBuffers_; index++){
    buffers_[index] = NULL;
  }
}

NDArrayRing2::~NDArrayRing2()
{
  clear();
  delete[] buffers_;
}

int NDArrayRing2::size()
{
  if (wrapped_ == 1){
    return noOfBuffers_;
  }
  return writeIndex_ + 1;
}

NDArray *NDArrayRing2::addToEnd(NDArray *pArray)
{
  NDArray *retVal = NULL;

  if (noOfBuffers_ > 0) {
      printf("\n[DEBUG] Before inserting NDArray into ring buffer, writeIndex_ = %i\n", writeIndex_);
      writeIndex_ = (writeIndex_ + 1) % noOfBuffers_;
      if (wrapped_ == 1){
          retVal = buffers_[writeIndex_];
      }
      buffers_[writeIndex_] = pArray;
      if (writeIndex_+1 == noOfBuffers_){
          // We have now wrapped
          wrapped_ = 1;
      } 

      if (size_ < noOfBuffers_)
        ++size_;
      printf("[DEBUG] Inserted NDArray into ring buffer, position (writeIndex_) %i, size = %i\n", writeIndex_, size_);
  
  } else {
      // Buffer is not being used, so return the passed array to be released immediately.
      retVal = pArray;
  }
  return retVal;
}

// Return the oldest frame in the buffer
NDArray *NDArrayRing2::readFromStart()
{
  if (wrapped_) {
    readIndex_ = ((writeIndex_+1) % noOfBuffers_);
  } else {
    readIndex_ = 0;
  }

  printf("[DEBUG] readFromStart(): Readindex = %d, writeIndex_ = %d\n", readIndex_, writeIndex_);
  return buffers_[readIndex_];
}

NDArray *NDArrayRing2::readNext()
{
  readIndex_ = (readIndex_ + 1) % noOfBuffers_;
  //printf("readNext - Readindex %d\n", readIndex_);
  return buffers_[readIndex_];
}

bool NDArrayRing2::hasNext()
{
  // Here readIndex is index of last frame read out
  if (readIndex_ == writeIndex_){
    return false;
  }
  return true;
}

void NDArrayRing2::clear()
{
  writeIndex_ = -1;
  readIndex_  = -1;
  wrapped_ = 0;
  if (buffers_){
    for (int index = 0; index < noOfBuffers_; index++){
      if (buffers_[index]){
        buffers_[index]->release();
        buffers_[index] = NULL;
      }
    }
  }
}

void NDArrayRing2::removeLast()
{
  printf("[DEBUG] Entering removeLast()\n");
  if (size_ <= 0) return;

  int oldest;

  // find the oldest
  if (wrapped_) {
    oldest = ((writeIndex_ + 1) % noOfBuffers_);
  } else {
    oldest = 0;
  }
  
  printf("[DEBUG] Will delete NDArray from ring buffer, position (oldest) %i\n", oldest);
  // delete the oldest
  if (buffers_[oldest]){
      buffers_[oldest]->release();
      buffers_[oldest] = NULL;
  }
  printf("[DEBUG] Deleted NDArray from ring buffer, position (oldest) %i\n", oldest);

  //update the write index
  ++writeIndex_;
  --size_;

  // if (writeIndex_ == -1) {
  //   writeIndex_ = (wrapped_ == 1 ? (noOfBuffers_ - 1) : -1);
  // }
  //if (writeIndex_ == noOfBuffers_) writeIndex_ = -1;
  printf(" Now writeIndex_ == %i and size = %i\n", writeIndex_, size_);
  
}

int NDArrayRing2::hasItems()
{
  return size_;
}
