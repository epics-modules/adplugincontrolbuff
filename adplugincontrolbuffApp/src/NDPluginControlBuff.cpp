/*
 * NDPluginControlBuff.cpp
 *
 * Scope style triggered image recording
 * Author: Alan Greer
 *
 * Created June 21, 2013
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include <epicsMath.h>
#include <iocsh.h>

#include "NDPluginControlBuff.h"

#include <epicsExport.h>

static const char *driverName="NDPluginControlBuff";

#define DEFAULT_TRIGGER_CALC "0"

asynStatus NDPluginControlBuff::calculateTrigger(NDArray *pArray, int *trig)
{
    NDAttribute *trigger;
    char triggerString[256];
    double triggerValue;
    double calcResult;
    int status;
    int preTrigger, postTrigger, currentImage, triggered;
    static const char *functionName="calculateTrigger";

    *trig = 0;

    getIntegerParam(NDCtrlBuffPreTrigger,   &preTrigger);
    getIntegerParam(NDCtrlBuffPostTrigger,  &postTrigger);
    getIntegerParam(NDCtrlBuffCurrentImage, &currentImage);
    getIntegerParam(NDCtrlBuffTriggered,    &triggered);

    triggerCalcArgs_[0] = epicsNAN;
    triggerCalcArgs_[1] = epicsNAN;
    triggerCalcArgs_[2] = preTrigger;
    triggerCalcArgs_[3] = postTrigger;
    triggerCalcArgs_[4] = currentImage;
    triggerCalcArgs_[5] = triggered;

    getStringParam(NDCtrlBuffTriggerA, sizeof(triggerString), triggerString);
    trigger = pArray->pAttributeList->find(triggerString);
    if (trigger != NULL) {
        status = trigger->getValue(NDAttrFloat64, &triggerValue);
        if (status == asynSuccess) {
            triggerCalcArgs_[0] = triggerValue;
        }
    }
    getStringParam(NDCtrlBuffTriggerB, sizeof(triggerString), triggerString);
    trigger = pArray->pAttributeList->find(triggerString);
    if (trigger != NULL) {
        status = trigger->getValue(NDAttrFloat64, &triggerValue);
        if (status == asynSuccess) {
            triggerCalcArgs_[1] = triggerValue;
        }
    }

    setDoubleParam(NDCtrlBuffTriggerAVal, triggerCalcArgs_[0]);
    setDoubleParam(NDCtrlBuffTriggerBVal, triggerCalcArgs_[1]);
    status = calcPerform(triggerCalcArgs_, &calcResult, triggerCalcPostfix_);
    if (status) {
        asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
            "%s::%s error evaluating expression=%s\n",
            driverName, functionName, calcErrorStr(status));
        return asynError;
    }

    if (!isnan(calcResult) && !isinf(calcResult) && (calcResult != 0)) *trig = 1;
    setDoubleParam(NDCtrlBuffTriggerCalcVal, calcResult);

    return asynSuccess;
}


/** Callback function that is called by the NDArray driver with new NDArray data.
  * Stores the number of pre-trigger images prior to the trigger in a ring buffer.
  * Once the trigger has been received stores the number of post-trigger buffers
  * and then exposes the buffers.
  * \param[in] pArray  The NDArray from the callback.
  */
void NDPluginControlBuff::processCallbacks(NDArray *pArray)
{
    /*
     * It is called with the mutex already locked.  It unlocks it during long calculations when private
     * structures don't need to be protected.
     */
    int scopeControl, preCount, postCount, currentImage, currentPostCount, softTrigger;
    int presetTriggerCount, actualTriggerCount;
    NDArray *pArrayCpy = NULL;
    NDArrayInfo arrayInfo;
    int triggered = 0;

    //const char* functionName = "processCallbacks";

    /* Call the base class method */
    NDPluginDriver::beginProcessCallbacks(pArray);

    pArray->getInfo(&arrayInfo);

    // Retrieve the running state
    getIntegerParam(NDCtrlBuffControl,            &scopeControl);
    getIntegerParam(NDCtrlBuffPreTrigger,         &preCount);
    getIntegerParam(NDCtrlBuffPostTrigger,        &postCount);
    getIntegerParam(NDCtrlBuffCurrentImage,       &currentImage);
    getIntegerParam(NDCtrlBuffPostCount,          &currentPostCount);
    getIntegerParam(NDCtrlBuffSoftTrigger,        &softTrigger);
    getIntegerParam(NDCtrlBuffPresetTriggerCount, &presetTriggerCount);
    getIntegerParam(NDCtrlBuffPresetTriggerCount, &presetTriggerCount);
    getIntegerParam(NDCtrlBuffActualTriggerCount, &actualTriggerCount);

    // Are we running?
    if (scopeControl) {

      // Check for a soft trigger
      if (softTrigger){
        triggered = 1;
        setIntegerParam(NDCtrlBuffTriggered, triggered);
      } else {
        getIntegerParam(NDCtrlBuffTriggered, &triggered);
        if (!triggered) {
          // Check for the trigger based on meta-data in the NDArray and the trigger calculation
          calculateTrigger(pArray, &triggered);
          setIntegerParam(NDCtrlBuffTriggered, triggered);
        }
      }

      // First copy the buffer into our buffer pool so we can release the resource on the driver
      pArrayCpy = this->pNDArrayPool->copy(pArray, NULL, 1);

      if (pArrayCpy){

        // Have we detected a trigger event yet?
        if (!triggered) {
          // No trigger so add the NDArray to the pre-trigger ring
          pOldArray_ = preBuffer_->addToEnd(pArrayCpy);
          // If we overwrote an existing array in the ring, release it here
          if (pOldArray_){
            pOldArray_->release();
            pOldArray_ = NULL;
          }
          // Set the size
          setIntegerParam(NDCtrlBuffCurrentImage,  preBuffer_->size());
          if (preBuffer_->size() == preCount){
            setStringParam(NDCtrlBuffStatus,
                preCount ? "Buffer Wrapping" : "Dropping frames");
          }
        } else {
          // Trigger detected
          // Start making frames available if trigger has occured
          setStringParam(NDCtrlBuffStatus, "Flushing");

          // Has the trigger occurred on this frame?
          if (previousTrigger_ == 0){
            previousTrigger_ = 1;
            // Yes, so flush the ring first
            flushPreBuffer();
          }

          currentPostCount++;
          setIntegerParam(NDCtrlBuffPostCount,  currentPostCount);

          doCallbacksGenericPointer(pArrayCpy, NDArrayData, 0);
          if (pArrayCpy){
            pArrayCpy->release();
          }
        }

        // Stop recording once we have reached the post-trigger count, wait for a restart
        if (currentPostCount >= postCount){
          actualTriggerCount++;
          setIntegerParam(NDCtrlBuffActualTriggerCount, actualTriggerCount);
          if ((presetTriggerCount == 0) ||
              ((presetTriggerCount > 0) && (actualTriggerCount < presetTriggerCount)))
          {
            previousTrigger_ = 0;
            // Set the status to buffer filling
            setIntegerParam(NDCtrlBuffControl, 1);
            setIntegerParam(NDCtrlBuffSoftTrigger, 0);
            setIntegerParam(NDCtrlBuffTriggered, 0);
            setIntegerParam(NDCtrlBuffPostCount, 0);
            setStringParam(NDCtrlBuffStatus,
                preCount ? "Buffer filling" : "Dropping frames");
          } else {
            setIntegerParam(NDCtrlBuffTriggered, 0);
            setIntegerParam(NDCtrlBuffControl, 0);
            setStringParam(NDCtrlBuffStatus, "Acquisition Completed");
          }
        }
      } else {
        //printf("pArray NULL, failed to copy the data\n");
      }
    } else {
      // Currently do nothing
    }

    callParamCallbacks();
}

void NDPluginControlBuff::flushPreBuffer()
{
    if (NULL == preBuffer_) return;
    if (preBuffer_->size() > 0) {
      doCallbacksGenericPointer(preBuffer_->readFromStart(), NDArrayData, 0);
      while (preBuffer_->hasNext()) {
        doCallbacksGenericPointer(preBuffer_->readNext(), NDArrayData, 0);
      }
      preBuffer_->clear();
    }
}

void NDPluginControlBuff::controlledFlush()
{
    if (NULL == preBuffer_) return;
    
    printf("Reading and releasing the oldest NDArray\n");
    if (preBuffer_->hasItems() > 0) {
      printf("preBuffer_->hasItems() = %d\n", preBuffer_->hasItems());
      doCallbacksGenericPointer(preBuffer_->readFromStart(), NDArrayData, 0);
      preBuffer_->removeLast();
    }
}

/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus NDPluginControlBuff::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    int scopeControl, preCount;
    static const char *functionName = "writeInt32";

    if (function == NDCtrlBuffControl){
        if (value == 1){
          // If the control is turned on then create our new ring buffer
          getIntegerParam(NDCtrlBuffPreTrigger,  &preCount);
          if (preBuffer_){
            delete preBuffer_;
          }
          preBuffer_ = new NDArrayRing2(preCount);
          if (pOldArray_){
            pOldArray_->release();
          }
          pOldArray_ = NULL;

          previousTrigger_ = 0;

          // Set the status to buffer filling
          setIntegerParam(NDCtrlBuffSoftTrigger, 0);
          setIntegerParam(NDCtrlBuffTriggered, 0);
          setIntegerParam(NDCtrlBuffPostCount, 0);
          setIntegerParam(NDCtrlBuffActualTriggerCount, 0);
          setStringParam(NDCtrlBuffStatus,
              preCount ? "Buffer filling" : "Dropping frames");
        } else {
          // Control is turned off, before we have finished
          // Set the trigger value off, reset counter
          setIntegerParam(NDCtrlBuffSoftTrigger, 0);
          setIntegerParam(NDCtrlBuffTriggered, 0);
          setIntegerParam(NDCtrlBuffCurrentImage, 0);
          setStringParam(NDCtrlBuffStatus, "Acquisition Stopped");
        }

        // Set the parameter in the parameter library.
        status = (asynStatus) setIntegerParam(function, value);

    }  else if (function == NDCtrlBuffSoftTrigger){
        // Set the parameter in the parameter library.
        status = (asynStatus) setIntegerParam(function, value);

        // Set a soft trigger
        setIntegerParam(NDCtrlBuffTriggered, 1);

        epicsInt32 flushOn;
        getIntegerParam(NDCtrlBuffFlushOnSoftTrig, &flushOn);

        if (flushOn > 0){
            flushPreBuffer();
        }

    }  else if (function == NDCtrlBuffPreTrigger){
        getIntegerParam(NDCtrlBuffControl, &scopeControl);
        if (scopeControl) {
          setStringParam(NDCtrlBuffStatus, "Stop acquisition to set pre-count");
        } else if (value > (maxBuffers_ - 1)){
          // The value of pretrigger should not exceed max buffers
          setStringParam(NDCtrlBuffStatus, "Pre-count too high");
        } else if (value < 0) {
          setStringParam(NDCtrlBuffStatus, "Invalid pre-count value");
        } else {
          // Set the parameter in the parameter library.
          status = (asynStatus) setIntegerParam(function, value);
        }
    

    // Controlled flush
    } else if (function == NDCtrlBuffGetElement) {

      printf("Popping element ...\n");

      int command;
      getIntegerParam(NDCtrlBuffGetElement, &command);

      if (1) {
        setStringParam(NDCtrlBuffStatus, "Popping element");
        controlledFlush();
      }

      status = (asynStatus) setIntegerParam(function, 0);
    
    } else {
        // Set the parameter in the parameter library.
        status = (asynStatus) setIntegerParam(function, value);

        // If this parameter belongs to a base class call its method
        if (function < FIRST_NDPLUGIN_CTRL_BUFF_PARAM)
            status = NDPluginDriver::writeInt32(pasynUser, value);
    }

    // Do callbacks so higher layers see any changes
    status = (asynStatus) callParamCallbacks();

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, value=%d",
                  driverName, functionName, status, function, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%d\n",
              driverName, functionName, function, value);
    return status;
}

/** Called when asyn clients call pasynOctet->write().
  * This function performs actions for some parameters, including AttributesFile.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Address of the string to write.
  * \param[in] nChars Number of characters to write.
  * \param[out] nActual Number of characters actually written. */
asynStatus NDPluginControlBuff::writeOctet(asynUser *pasynUser, const char *value, size_t nChars, size_t *nActual)
{
  int addr=0;
  int function = pasynUser->reason;
  asynStatus status = asynSuccess;
  short postfixError;
  const char *functionName = "writeOctet";

  status = getAddress(pasynUser, &addr); if (status != asynSuccess) return(status);
  // Set the parameter in the parameter library.
  status = (asynStatus)setStringParam(addr, function, (char *)value);
  if (status != asynSuccess) return(status);

  if (function == NDCtrlBuffTriggerCalc){
    if (nChars > sizeof(triggerCalcInfix_)) nChars = sizeof(triggerCalcInfix_);
    // If the input string is empty then use a value of "0", otherwise there is an error
    if ((value == 0) || (strlen(value) == 0)) {
      strcpy(triggerCalcInfix_, DEFAULT_TRIGGER_CALC);
      setStringParam(NDCtrlBuffTriggerCalc, DEFAULT_TRIGGER_CALC);
    } else {
      strncpy(triggerCalcInfix_, value, sizeof(triggerCalcInfix_));
    }
    status = (asynStatus)postfix(triggerCalcInfix_, triggerCalcPostfix_, &postfixError);
    if (status) {
      asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
      "%s::%s error processing infix expression=%s, error=%s\n",
      driverName, functionName, triggerCalcInfix_, calcErrorStr(postfixError));
    }
  }

  else if (function < FIRST_NDPLUGIN_CTRL_BUFF_PARAM) {
      /* If this parameter belongs to a base class call its method */
      status = NDPluginDriver::writeOctet(pasynUser, value, nChars, nActual);
  }

  // Do callbacks so higher layers see any changes
  callParamCallbacks(addr, addr);

  if (status){
    epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
                  "%s:%s: status=%d, function=%d, value=%s",
                  driverName, functionName, status, function, value);
  } else {
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
              "%s:%s: function=%d, value=%s\n",
              driverName, functionName, function, value);
  }
  *nActual = nChars;
  return status;
}

/** Constructor for NDPluginControlBuff; most parameters are simply passed to NDPluginDriver::NDPluginDriver.
  * After calling the base class constructor this method sets reasonable default values for all of the
  * parameters.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
NDPluginControlBuff::NDPluginControlBuff(const char *portName, int queueSize, int blockingCallbacks,
                         const char *NDArrayPort, int NDArrayAddr,
                         int maxBuffers, size_t maxMemory,
                         int priority, int stackSize)
    /* Invoke the base class constructor */
    : NDPluginDriver(portName, queueSize, blockingCallbacks,
                   NDArrayPort, NDArrayAddr, 1, maxBuffers, maxMemory,
                   asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
                   asynInt32ArrayMask | asynFloat64ArrayMask | asynGenericPointerMask,
                   0, 1, priority, stackSize, 1), pOldArray_(NULL)
{
    //const char *functionName = "NDPluginControlBuff";
    preBuffer_ = NULL;

    maxBuffers_ = maxBuffers;

    // Scope
    createParam(NDCtrlBuffControlString,            asynParamInt32,      &NDCtrlBuffControl);
    createParam(NDCtrlBuffStatusString,             asynParamOctet,      &NDCtrlBuffStatus);
    createParam(NDCtrlBuffTriggerAString,           asynParamOctet,      &NDCtrlBuffTriggerA);
    createParam(NDCtrlBuffTriggerBString,           asynParamOctet,      &NDCtrlBuffTriggerB);
    createParam(NDCtrlBuffTriggerAValString,        asynParamFloat64,    &NDCtrlBuffTriggerAVal);
    createParam(NDCtrlBuffTriggerBValString,        asynParamFloat64,    &NDCtrlBuffTriggerBVal);
    createParam(NDCtrlBuffTriggerCalcString,        asynParamOctet,      &NDCtrlBuffTriggerCalc);
    createParam(NDCtrlBuffTriggerCalcValString,     asynParamFloat64,    &NDCtrlBuffTriggerCalcVal);
    createParam(NDCtrlBuffPresetTriggerCountString, asynParamInt32,      &NDCtrlBuffPresetTriggerCount);
    createParam(NDCtrlBuffActualTriggerCountString, asynParamInt32,      &NDCtrlBuffActualTriggerCount);
    createParam(NDCtrlBuffPreTriggerString,         asynParamInt32,      &NDCtrlBuffPreTrigger);
    createParam(NDCtrlBuffPostTriggerString,        asynParamInt32,      &NDCtrlBuffPostTrigger);
    createParam(NDCtrlBuffCurrentImageString,       asynParamInt32,      &NDCtrlBuffCurrentImage);
    createParam(NDCtrlBuffPostCountString,          asynParamInt32,      &NDCtrlBuffPostCount);
    createParam(NDCtrlBuffSoftTriggerString,        asynParamInt32,      &NDCtrlBuffSoftTrigger);
    createParam(NDCtrlBuffTriggeredString,          asynParamInt32,      &NDCtrlBuffTriggered);
    createParam(NDCtrlBuffFlushOnSoftTrigString,    asynParamInt32,      &NDCtrlBuffFlushOnSoftTrig);
    createParam(NDCtrlBuffGetElementString,         asynParamInt32,      &NDCtrlBuffGetElement);

    // Set the plugin type string
    setStringParam(NDPluginDriverPluginType, "NDPluginControlBuff");

    // Set the status to idle
    setStringParam(NDCtrlBuffStatus, "Idle");

    // Init the current frame count to zero
    setIntegerParam(NDCtrlBuffCurrentImage, 0);
    setIntegerParam(NDCtrlBuffPostCount, 0);

    // Init the scope control to off
    setIntegerParam(NDCtrlBuffControl, 0);

    // Init the pre and post count to 100
    setIntegerParam(NDCtrlBuffPreTrigger, 100);
    setIntegerParam(NDCtrlBuffPostTrigger, 100);

    // Init the preset trigger count to 1
    setIntegerParam(NDCtrlBuffPresetTriggerCount, 1);
    setIntegerParam(NDCtrlBuffActualTriggerCount, 0);

    setIntegerParam(NDCtrlBuffFlushOnSoftTrig, 0);

    setIntegerParam(NDCtrlBuffGetElement, 0);

    // Enable ArrayCallbacks.
    // This plugin currently ignores this setting and always does callbacks, so make the setting reflect the behavior
    setIntegerParam(NDArrayCallbacks, 1);

    // Try to connect to the array port
    connectToArrayPort();
}

/** Configuration command */
extern "C" int NDControlBuffConfigure(const char *portName, int queueSize, int blockingCallbacks,
                                const char *NDArrayPort, int NDArrayAddr,
                                int maxBuffers, size_t maxMemory)
{
    NDPluginControlBuff *pPlugin = new NDPluginControlBuff(portName, queueSize, blockingCallbacks, NDArrayPort, NDArrayAddr,
                                                             maxBuffers, maxMemory, 0, 2000000);
    return pPlugin->start();
}

/* EPICS iocsh shell commands */
static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArrayPort",iocshArgString};
static const iocshArg initArg4 = { "NDArrayAddr",iocshArgInt};
static const iocshArg initArg5 = { "maxBuffers",iocshArgInt};
static const iocshArg initArg6 = { "maxMemory",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6};
static const iocshFuncDef initFuncDef = {"NDControlBuffConfigure",7,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    NDControlBuffConfigure(args[0].sval, args[1].ival, args[2].ival,
                     args[3].sval, args[4].ival, args[5].ival,
                     args[6].ival);
}

extern "C" void NDControlBuffRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDControlBuffRegister);
}


